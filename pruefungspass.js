const rezepte = ref([
    {
        "rezeptId": 1,
        "title": "hallo Welt"
    }
])

const zutaten = ref([
    {
        "zutatId": 1,
        "rezeptId": 1
    },
    {
        "zutatId": 2,
        "rezeptId": 1
    }
])

const rezeptId = ref(1)
const zutatenFürRezept = computed(() => zutaten.value.filter(zutat => zutat.rezeptId === rezeptId.value))
