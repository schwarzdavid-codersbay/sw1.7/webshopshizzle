import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {API_URL} from "@/api";

export const useProductStore = defineStore('products', () => {
    const products = ref([])

    async function loadAllProducts() {
        const productResponse = await axios.get(API_URL + 'products')
        products.value = productResponse.data
    }

    return {
        products,
        loadAllProducts
    }
})
